notice('===  Testing Landscape ===')

$host_ip    = '192.168.33.12'
$servername = 'landscape'

host { $servername:
  ip => $host_ip,
}
class { 'landscape':
  servername => $servername,
  require    => Host[$servername],
}
class { 'landscape::client':
  server_url         => $servername,
  copy_server_cert   => false,
  server_cert_path   => '/etc/ssl/certs/landscape_server_ca.crt',
  enable_script_exec => true,
  require            => Class['landscape'],
}
