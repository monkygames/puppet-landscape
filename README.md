# ![landscape](https://bitbucket.org/monkygames/puppet-landscape/downloads/logo.png) Landscape Puppet Module

## Overview

Deploys and manages the Landscape Standalone Server as well as deploys landscape clients.
Verison [17.03](https://help.landscape.canonical.com/LDS/QuickstartDeployment17.03) of the documenation was used for this puppet module.

See Updated documentation for Version [19.10](https://docs.ubuntu.com/landscape/en/landscape-install-quickstart).

## Requirements

The following are requirements for running the Landscape Server.

* CPU: Dual-Core 2.0GHz or higher
* RAM: 4GB
* HDD: 5GB
* OS: Ubuntu Server 18.04 64-bit or higher
* Network: ports 80 & 443 open

## Api

See REFERENCE.md

## Limitations

Works with Debian Based OS's only.
Also, this module only works with LDS version 17.x and newer.

Note, as of 2021-07, landscape server doesn't have a package for Ubuntu 20.04.  So you must use Ubuntu 18.04 server if you want to install the landscape server.  There is support for clients though of all Ubuntu versions.
