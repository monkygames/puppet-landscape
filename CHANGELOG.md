# Landscape Puppet Module Changelog

## 2022-11-10 Version 0.3.1

- Fixed a merge error.

## 2022-11-10 Version 0.3.0

- Updated documentation.
- Updated PDK and fixed issues detected by linter.
- Added a changelog and backfilled.
- Updated apache module version which switched to newer data types for parameters.
- Updated default verison of landscape.

## 2021-07-30 Version 0.2.2

- Fixed medata file.

## 2021-07-29 Version 0.2.1

- Added parameter to remove client packages and files.
- Added changelog

## 2017-07-03 Version 0.2.0

- Fixed an issue and also updated doco about pam possibly not working.
- A major upgrade of this module for LDS 17.x

## 2016-02-13 Version 0.1.5

- Updated documentation and addedd contrib file.
- Added extra_config to client configuration.

## 2015-10-13 Version 0.1.4

- Updated for future parser compatibility.

## 2015-10-12 Version 0.1.3 & 0.1.2

- Updated metadata documentation.

## 2015-10-12 Version 0.1.1

- Initial release.
