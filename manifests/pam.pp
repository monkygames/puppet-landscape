# @summary Manages the moinmoin plugin for ldap integration.
#
class landscape::pam (
) {
  file { '/etc/pam.d/landscape':
    ensure => file,
    source => 'puppet:///modules/landscape/landscape.pam',
  }
}
