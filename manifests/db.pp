# @summary Manages the db for landscape.
#
# @param version
#   The version of the postgresql database to install.
#
class landscape::db (
  String $version = '9.6'
) {
  $packages = ['python-smartpm',"postgresql-plpython-${version}"]

  # additional packages to be installed
  package { $packages:
    ensure => present,
  }
}
