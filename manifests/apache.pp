# @summary Manages the apache specific configuration.
#
# @param servername
#   The server's url.
#
# @param ssl_cert_path
#   The ssl cert path for apache.
#
# @param ssl_key_path
#   The key to the cert for apache.
#
# @param ssl_chain_path
#   The chain for the apache certs.
#
class landscape::apache (
  String $servername,
  String $ssl_cert_path,
  String $ssl_key_path,
  String $ssl_chain_path,
) {
  # port 80 which should redirect to port 443 on most cases
  apache::vhost { 'landscape':
    docroot     => '/opt/canonical/landscape/canonical/landscape',
    servername  => $servername,
    port        => 80,
    aliases     => [
      {
        alias => '/offline',
        path  => '/opt/canonical/landscape/canonical/landscape/offline'
      },
      {
        alias => '/static',
        path  => '/opt/canonical/landscape/canonical/static'
      },
      {
        alias => '/repository',
        path  => '/var/lib/landscape/landscape-repository'
      },
    ],
    rewrites    => [
      {
        rewrite_rule => '^/r/([^/]+)/(.*) /$2',
      },
      { comment      => '# The Landscape Ping Server runs on port 8070',
        rewrite_rule => ['^/ping$ http://localhost:8070/ping [P]'],
      },
      { rewrite_cond => [
          '%{REQUEST_URI} !^/server-status',
          '%{REQUEST_URI} !^/icons',
          '%{REQUEST_URI} !^/static/',
          '%{REQUEST_URI} !^/offline/',
          '%{REQUEST_URI} !^/repository/',
          '%{REQUEST_URI} !^/message-system',
        ],
        rewrite_rule => ["^/(.*) https://${servername}:443/\$1 [R=permanent]"],
      }
    ],
    directories => [
      { path            => '/',
        options         => ['+Indexes'],
        auth_require    => 'all granted',
        error_documents => [
          {
            error_code => '403',
            document   => '/offline/unauthorized.html',
          },
          {
            error_code => '404',
            document   => '/offline/notfound.html',
          },
        ],
      },
      { path            => '/repository',
        provider        => 'location',
        auth_require    => 'all denied',
        error_documents => [
          {
            error_code => '403',
            document   => 'default',
          },
          {
            error_code => '404',
            document   => 'default',
          },
        ]
      },
      { path         => '/repository/[^/]+/[^/]+/(dists|pool)/.*',
        provider     => 'locationmatch',
        auth_require => 'all granted'
      },
      { path         => '/icons',
        provider     => 'location',
        auth_require => 'all granted',
      },
      { path         => '/ping',
        provider     => 'location',
        auth_require => 'all granted',
      },
      { path         => '/message-system',
        provider     => 'location',
        auth_require => 'all granted',
      },
      { path     => '/static',
        provider => 'location',
        headers  => 'always append X-Frame-Options SAMEORIGIN',
      },
      { path            => '/r',
        provider        => 'location',
        headers         => 'append Cache-Control "public',
        custom_fragment => "FileETag none\n\
ExpiresActive on\n\
ExpiresDefault \"access plus 10 years\""
      },
    ],
  }

  apache::vhost { 'landscape-ssl':
    docroot             => '/opt/canonical/landscape/canonical/landscape',
    servername          => $servername,
    port                => 443,
    ssl                 => true,
    ssl_proxyengine     => true,
    ssl_cert            => $ssl_cert_path,
    ssl_key             => $ssl_key_path,
    ssl_chain           => $ssl_chain_path,
    proxy_preserve_host => true,
    aliases             => [
      { alias => '/config',
        path  => '/opt/canonical/landscape/apacheroot',
      },
      { alias => '/hash-id-databases',
        path  => '/var/lib/landscape/hash-id-databases'
      },
      { alias => '/offline',
        path  => '/opt/canonical/landscape/canonical/landscape/offline'
      },
    ],
    proxy_pass          => [
      {
        path => '/robots.txt',
        url  => '!'
      },
      {
        path => '/favicon.ico',
        url  => '!'
      },
      {
        path => '/static',
        url  => '!'
      },
      {
        path => '/offline',
        url  => '!'
      },
    ],
    directories         => [
      {
        path            => '/',
        options         => ['-Indexes'],
        auth_require    => 'all granted',
        error_documents => [
          {
            error_code => '403',
            document   => '/offline/unauthorized.html',
          },
          {
            error_code => '404',
            document   => '/offline/notfound.html',
          },
        ],
      },
      { path     => '/static',
        provider => 'location',
        headers  => 'always append X-Frame-Options SAMEORIGIN',
      },
      { path            => '/r',
        provider        => 'location',
        headers         => 'append Cache-Control "public"',
        custom_fragment => "FileETag none\n\
ExpiresActive on\n\
ExpiresDefault \"access plus 10 years\"",
      },
      { path         => '/ajax',
        provider     => 'location',
        auth_require => 'all granted',
      },
      { path         => '/message-system',
        provider     => 'location',
        auth_require => 'all granted',
      },
      {
        path            => '/',
        provider        => 'location',
        headers         => 'append Vary User-Agent env=!dont-vary',
        custom_fragment => template('landscape/output_filter.erb'),
      }
    ],
    custom_fragment     => template('landscape/proxy_directive.erb'),
    rewrites            => [
      {
        rewrite_rule => ['^/.*\+\+.* / [F]', '^/r/([^/]+)/(.*) /$2'],
      },
      {
        rewrite_rule => ["^/message-system http://localhost:8090/++vh++https:${servername}:443/++/ [P,L]"],
      },
      {
        rewrite_rule => [
          '^/ajax http://localhost:9090/ [P]',
          '^/combo(.*) http://localhost:8080/combo$1 [P,L]',
          '^/api http://localhost:9080/ [P,L]',
          '^/attachment/(.*) http://localhost:8090/attachment/$1 [P,L]',
          '^/upload/(.*) http://localhost:9100/$1 [P,L]',
        ],
      },
      {
        rewrite_cond => [
          '%{REQUEST_URI} !/robots.txt$',
          '%{REQUEST_URI} !/favicon.ico$',
          '%{REQUEST_URI} !^/(r/[^/]+/)?static/',
          '%{REQUEST_URI} !/offline/',
          '%{REQUEST_URI} !/config/',
          '%{REQUEST_URI} !/hash-id-databases/',
        ],
        rewrite_rule => ["^/(.*) http://localhost:8080/++vh++https:${servername}:443/++/\$1 [P]"],
      }
    ],
  }
}
