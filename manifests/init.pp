# @summary Installs the landscape server.
#
# @param servername
#   The server's url.
#
# @param license
#   Contains the license file contents.
#
# @param version
#  The server version to install landscape.
#
# @param ssl_cert_path
#   The ssl cert path for apache.
#   The default cert is installed by the landscape package.
#
# @param ssl_key_path
#   The key to the cert for apache.
#
# @param ssl_chain_path
#   The chain for the apache certs.
#
# @param enable_pam
#   Uses PAM to authenticate users. Note, this module does not
#   setup PAM for authentication.  It only uses the existing pam
#   configuration to authenticate users.
#
class landscape (
  String           $servername,
  Optional[String] $license        = undef,
  String           $version        = '19.10',
  String           $ssl_cert_path  = '/etc/ssl/certs/landscape_server.pem',
  String           $ssl_key_path   = '/etc/ssl/private/landscape_server.key',
  String           $ssl_chain_path = '/etc/ssl/certs/landscape_server_ca.crt',
  Boolean          $enable_pam     = false,
) {
  $apache_conf     = "/etc/apache2/sites-available/${facts['networking']['hostname']}.conf"

  $required_packages = ['software-properties-common','ruby']
  ensure_packages($required_packages)

  # add the ppa
  include apt
  apt::ppa { "ppa:landscape/${version}":
    require => Package[$required_packages],
  }

  exec { 'apt_update_landscape':
    command     => '/usr/bin/apt-get update',
    refreshonly => true,
    subscribe   => Apt::Ppa["ppa:landscape/${version}"],
  }

  package { 'landscape-server-quickstart':
    require => Exec['apt_update_landscape'],
  }

  if $enable_pam {
    class { 'landscape::pam':
      require => Package['landscape-server-quickstart'],
      before  => Class['apache'],
    }
  }

  class { 'apache':
    mpm_module    => 'prefork',
    default_vhost => false,
    require       => Package['landscape-server-quickstart'],
  }

  contain apache::mod::rewrite
  contain apache::mod::ssl
  contain apache::mod::proxy_http
  contain apache::mod::proxy
  contain apache::mod::expires
  contain apache::mod::headers

  class { 'landscape::apache':
    servername     => $servername,
    ssl_cert_path  => $ssl_cert_path,
    ssl_key_path   => $ssl_key_path,
    ssl_chain_path => $ssl_chain_path,
    require        => Package['landscape-server-quickstart'],
  }

  # setup the license if it exists.
  if $license {
    file { '/etc/landscape/license.txt':
      ensure  => file,
      content => $license,
      require => Package['landscape-server-quickstart'],
      notify  => Exec['restart_landscape'],
    }
  }

  exec { 'restart_landscape':
    command     => "/usr/bin/lsctl stop && \
/usr/sbin/service apache2 restart && \
/usr/bin/lsctl start",
    refreshonly => true,
    subscribe   => Class['apache'],
    require     => Class['landscape::apache'],
  }
}
