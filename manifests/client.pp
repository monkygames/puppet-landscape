# @summary Manages a client for landscape.
#
# @param server_url
#   The landscape server's FQDN.
#
# @param ensure
#   The following states.
#   * present - software installed and configured.
#   * absent  - the software and configuration removed 
#
# @param server_cert
#   The landscape server certificate contents.
#
# @param server_cert_path
#   The full path to the server's certificate.
#
# @param copy_server_cert
#   If set to false, assumes the certificate is already copied.
#   If set to true, uses the server_cert to fill the contents of the cert.
#
# @param account_name
#   The account this computer belongs to.
#
# @param computer_title
#   The title of this computer.
#
# @param enable_script_exec
#   Set to true if landscape can run scripts remotely.
#
# @param script_users
#   A string of a comma separated list of users that can run scripts.
#
# @param extra_config
#   A hash containing additional configuration parameters.
#
class landscape::client (
  String           $server_url,
  String           $ensure             = 'present',
  Optional[String] $server_cert        = undef,
  String           $server_cert_path   = '/etc/landscape/server.pem',
  Boolean          $copy_server_cert   = true,
  String           $account_name       = 'standalone',
  String           $computer_title     = $facts['networking']['hostname'],
  Boolean          $enable_script_exec = false,
  String           $script_users       = 'root',
  Hash             $extra_config       = {},
) {
  if $ensure == 'present' {
    package { 'landscape-client':
      ensure => present,
    }
  }

  if $copy_server_cert {
    file { $server_cert_path:
      ensure  => file,
      content => $server_cert,
      require => Package['landscape-client'],
      before  => File['/etc/landscape/client.conf'],
    }

    file { '/etc/default/landscape-client':
      ensure  => file,
      mode    => '0644',
      content => "RUN=1\n",
      require => Package['landscape-client'],
    }

    file { '/etc/landscape/client.conf':
      ensure  => file,
      content => template('landscape/client.conf.erb'),
      owner   => 'landscape',
      group   => 'root',
      mode    => '0600',
      require => File['/etc/default/landscape-client'],
    }

    service { 'landscape-client':
      ensure     => running,
      hasrestart => true,
      hasstatus  => true,
      enable     => true,
      subscribe  => File['/etc/landscape/client.conf'],
    }
  } else {
    package { 'landscape-client':
      ensure => absent,
    }

    file { $server_cert_path:
      ensure  => absent,
    }

    file { '/etc/default/landscape-client':
      ensure  => absent,
    }

    file { '/etc/landscape/client.conf':
      ensure  => absent,
    }
  }
}
